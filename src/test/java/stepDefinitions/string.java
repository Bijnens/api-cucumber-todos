package stepDefinitions;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;

public class example_apiTestSteps {
    String apiUrl = "https://jsonplaceholder.typicode.com/todos/";
    HttpResponse<JsonNode> firstTodoResponse;

    @Given("^string$")
    public void the_todo_api_is_up() throws UnirestException {
        
    }

    @When("^string is iets$")
    public void i_request_the_first_todo() throws  UnirestException {
        
    }

    @Then("^string is not empty$")
    public void the_first_todo_is_returned() {
        
    }
}
